function [time_filter, company_filter] = biggest_block(nan_compensation)
    load('fundamentals_returns.mat');
    

    roa = IBQ ./ ATQ;
    ninc = NIQ ./ ATQ;
    cf = (DPQ + IBQ) ./ ATQ;
    seqq = SEQQ ./ ATQ;
    debt = DLTTQ ./ ATQ;
    opmargin = OIADPQ ./ REVTQ;

    nan_matrix = roa + ninc + cf + seqq + debt + opmargin + RETS;

    nan_matrix(nan_matrix == inf | nan_matrix == -inf) = nan;

    base_matrix = ~isnan(nan_matrix);

    num_of_companies = zeros(1,size(nan_matrix,1));
    ind = zeros(1,size(nan_matrix,1));
    for i = 1:size(nan_matrix, 1)
        m = [];
        for j = 1:size(nan_matrix, 1)
            if (j + i - 1 > size(nan_matrix, 1))
               break; 
            end
           test_matrix = base_matrix(j:j+i - 1, :);
           a = sum(test_matrix, 1) >= i - nan_compensation;
           companies = sum(a);
           m = [m, companies];
        end

        [num_of_companies(i), ind(i)] = max(m);
    end
    temp = 1:size(nan_matrix);
    data_pts = temp .* num_of_companies;

    [tempc i] = max(data_pts);
    j = ind(i);
    j + i - 1 - j + 1
    test_matrix = base_matrix(j:j+i-1, :);
    company_filter = sum(test_matrix, 1) >= i - nan_compensation;
    time_filter = (j:j+i - 1)';
end
