
function rank_matrix = quarter_rank(x, y, GICS)
    sec = unique(GICS(:,3));
    sec = sec(sec > 0);
    
    rank_matrix = zeros(size(x, 1), length(sec) + 1);
    %for each sector
    for i = 1:length(sec)
       mat1 = x(:, GICS(:, 3) == sec(i));
       mat2 = y(:, GICS(:, 3) == sec(i));
       
       rank_matrix(:, i) = rank(mat1, mat2);
       
    end
end

function kendall = rank(x, y)
    kendall = zeros(size(x, 1), 1);
    samples = zeros(size(x, 1), 1);
    for i = 1:size(x, 1)
       [vec1, vec2] = remove_nans(x(i,:)', y(i,:)');
       samples(i) = length(vec1);
       if (length(vec1) > 0)
            kendall(i) = corr(vec1, vec2, 'type', 'Kendall');
       end
    end
    kendall = kendall(~isnan(kendall));
end
