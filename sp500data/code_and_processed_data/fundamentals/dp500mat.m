
% parameters:
% return_data:          returns for all companies in csv format.
%                       format: date | permno | return
% constituents_filter:  List of required companies companies (LPERMNO)
% quarters:             End of quarter dates
% avg_delay:            mean delay between quarter end date and publish
%                       date
% Output:
% RETS:                 Accumulated returns per quarter (row) per company
%                       (Column) offest by avg_delay
function RETS = dp500mat(return_data, constituents_filter, quarters, avg_delay)
    
    %read returns file

    [dates, permno, returns] = textread(return_data, '%s %d %f', 'delimiter', ',');
    dates = datenum(dates, 'yyyy-mm-dd');
    
    % special crsp codes when trading was not done
    special_numbers = [-66.0, -77.0, -99.0];
    unique_dates = unique(dates);
    
    %we will process all the dates including weekends and holidays and
    %later remove them. This will allow better indexing in the loop later.
    all_dates = unique_dates(1):unique_dates(length(unique_dates));
    % starting date
    index_offset = unique_dates(1);

    % pre-allocate memory for return matrix.
    % one column per company
    % one row per day
    return_matrix = ones(length(all_dates), 1) * nan;
    companies = [];
    prev_comp = permno(1);
    
    j = 1;
    companies(j) = prev_comp;

    for i = 1:length(returns)
        curr_comp = permno(i);
        % if company changed, switch to next column
        if (curr_comp > prev_comp)
            j = j + 1;
            companies(j) = permno(i);
            prev_comp = permno(i);
            return_matrix = [return_matrix, ones(length(all_dates), 1) * nan];
        end
        
        %put return value.
        return_matrix(dates(i) - index_offset + 1, j) = returns(i);
        
    end
    
    %filter out the unwanted companies
    filter = ismember(companies, constituents_filter); 
    return_matrix = return_matrix(:, filter);
    
    % deal with special numbers
    return_matrix(ismember(return_matrix, special_numbers)) = nan;
    
    % Accumulate returns per quarter (JAN - DEC)
    RETS = ones(length(quarters), sum(filter)) * nan;
    
    for i = 1:length(quarters)-1
            qtr_base = quarters(i) - index_offset + 1 + avg_delay;
            if qtr_base < 1
                qtr_base = 1;
            end
            qtr_end = quarters(i+1) - index_offset + avg_delay;
            
            if ((qtr_end + index_offset) > unique_dates(length(unique_dates)))
                qtr_end = unique_dates(length(unique_dates)) - index_offset; 
            end
            
            if ((qtr_base + index_offset) > unique_dates(length(unique_dates)))
                qtr_base = unique_dates(length(unique_dates)) - index_offset; 
            end
            
            if (qtr_base ~= qtr_end)
                qtr_ret = return_matrix(qtr_base:qtr_end,:);
                %remove weekends and holidays
                qtr_ret = qtr_ret(ismember(qtr_base+index_offset:qtr_end+index_offset, unique_dates), :);
                nan_cnt = sum(isnan(qtr_ret));
                RETS(i,:) = nansum(qtr_ret);
                % if more than 15 nan in a quarter, then ignore the quarter
                RETS(i, nan_cnt > 15) = nan;
            end
    end
    
end