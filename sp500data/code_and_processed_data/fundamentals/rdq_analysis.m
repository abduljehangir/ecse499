% returns the MAX delay in publish date per quarter

function diff = rdq_analysis(RDQ, CDATES)

    for i = 2:size(RDQ)
        a = RDQ(i,:);
        %ignore values which are equal to 2013-12-31
        a(find(a==735965)) = nan;
        maxrdq(i, 1) = max(a);
    end
    diff = maxrdq - CDATES + 1;
    diff = diff(diff >= 0);

end