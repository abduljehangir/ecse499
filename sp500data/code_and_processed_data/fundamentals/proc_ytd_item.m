function output = proc_ytd_item(input)
    item = input(2:size(input,1),:);
    
    output = ones(size(item,1), size(item, 2)) * nan;
    %take starting index of each year
    %compute quarterly data
    for i = 1:4:size(item,1);
        output(i,:) = item(i, :);
        output(i+1,:) = item(i+1,:) - item(i,:);
        output(i+2,:) = item(i+2,:) - item(i+1,:);
        output(i+3,:) = item(i+3,:) - item(i+2,:);
    end
    
    %add the first row removed in the first line
    output = [ones(1, size(item,2))*nan; output];
end

