filename = 'cf_fiscal_change_removed.csv'
[GVKEY, LPERMNO, TIC, COMPNAME, CDATES, RDQ, ATQ, DLTTQ, DPQ, DVPQ, EPSFIQ, EPSFXQ, EPSPIQ, EPSPXQ, IBQ, INTANQ, ITQ, NIQ, OIADPQ, OIBDPQ, REQ, REVTQ, SALEQ, SEQQ, TEQQ, DVY, DVPSPQ, DVPSXQ, MKVALTQ, PRCCQ, PRCHQ, PRCLQ, GIC, FISCAL_END, IVNCFY, OANCFY] = proc_fundamentals(filename);
filename = 'return_data_sp500.csv'
RETS = dp500mat(filename, LPERMNO, CDATES, 194);

%process year to date items
OANCFQ = proc_ytd_item(OANCFY);
IVNCFQ = proc_ytd_item(IVNCFY);
DVQ = proc_ytd_item(DVY);

clear filename
clear OANCFY
clear IVNCFY
clear DVY