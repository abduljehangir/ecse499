function [X, Y] = remove_nans(x, y)
   filtered_rows = (isnan(x) == 0) & (isnan(y) == 0);
    %eliminate nans
   X = x(filtered_rows);
   Y = y(filtered_rows);
   
end