function data_pts = valid_data_pts(x, rets)
   data_pts = zeros(size(rets,2), 1);
   
   for i = 1:size(rets, 2)
       data_pts(i) = sum(~isnan(x(:, i)) & ~isnan(rets(:, i)));
   end
end