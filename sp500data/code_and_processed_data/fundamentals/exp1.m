function [tau, S] = exp1()
    load('fundamentals_returns.mat')
    
    N = 1000;
    % Compute Fundamentals and Normalize
    roa = IBQ ./ ATQ;
    ninc = NIQ ./ ATQ;
    cf = (OANCFQ + IVNCFQ) ./ ATQ;
    seqq = SEQQ ./ ATQ;
    debt = -1 * (DLTTQ ./ ATQ);
    opmargin = OIADPQ ./ REVTQ;
    
    %tau = ones(size(roa, 1),1) * 999999;
    %S = ones(size(roa, 1),1) * 999999;
    tau = [];
    S = [];
    
    predict = 0;
    
    for i = 1:size(roa, 1)
        % update Y
        Y = RETS(i, :)';
        X = [roa(i, :)' ninc(i, :)' cf(i, :)' seqq(i, :)' debt(i, :)' opmargin(i, :)'];
        
        [X, Y] = remove_nans(X, Y);
        
        if length(Y) > 0
           if predict == 1
               tau_t = corr(X*u, Y*v, 'type', 'Kendall');
               tau = [tau; tau_t];
               S = [S; significance(X, Y, u, tau_t, N)];
           else
               predict = 1;
           end
           
          [u, v, temp1, temp2] = SCRCA(X, Y, 10);
          
        end
        i / size(roa,1) * 100
    end
    
end


function [X, Y] = remove_nans(X, Y)
    %remove nans in each row of X and Y
   X(X == -inf | X == inf) = nan;
   filter = ~isnan(sum(X,2)) & ~isnan(Y);
   X = X(filter,:);
   Y = Y(filter);
end

function S = significance(X, Y, u, tau, N)
    
    Y_perm = zeros(length(Y), N);
    %take N different permulations of Y
    for i = 1:N
       Y_perm(:,i) = shuffle(Y); 
    end
    %find correlation wrt each column of Y
    tau_p = corr(X*u, Y_perm, 'type', 'Kendall');
    
    S = sum(abs(tau) > abs(tau_p)) / N;
end

function y = shuffle(Y)
    y = Y(randperm(length(Y)));
end