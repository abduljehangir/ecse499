function [GVKEY, LPERMNO, TIC, COMPNAME, CDATES, RDQ, ATQ, DLTTQ, DPQ, DVPQ, EPSFIQ, EPSFXQ, EPSPIQ, EPSPXQ, IBQ, INTANQ, ITQ, NIQ, OIADPQ, OIBDPQ, REQ, REVTQ, SALEQ, SEQQ, TEQQ, DVY, DVPSPQ, DVPSXQ, MKVALTQ, PRCCQ, PRCHQ, PRCLQ, GIC, FISCAL_END, IVNCFY, OANCFY] = proc_fundamentals(filename)

    [gvkey, lpermno, datadate, fyearq, fqtr, fyr, tic, compname, datacqtr, datafqtr, rdq, atq, dlttq, dpq, dvpq, epsfiq, epsfxq, epspiq, epspxq, ibq, intanq, itq, niq, oiadpq, oibdpq, req, revtq, saleq, seqq, teqq, dvy, ivncfy, oancfy, dvpspq, dvpsxq, mkvaltq, prccq, prchq, prclq, gsector, ggroup, gind, gsubind] = textread(filename, '%d %d %s %d %d %d %s %s %s %s %s %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %d %d %d %d', 'delimiter', ',');

    %define matrix to calculate fiscal year end month
    %fiscal_def = [10,11,12,1,2,3,4,5,6,7,8,9;7,8,9,10,11,12,1,2,3,4,5,6;4,5,6,7,8,9,10,11,12,1,2,3;1,2,3,4,5,6,7,8,9,10,11,12];
    
    datadate = char(datadate);
    %convert to numerical dates
    datadates = datenum(datadate, 'yyyymmdd');
    datacqtr = datenum(datacqtr, 'yyyymmdd');
    datafqtr = datenum(datafqtr, 'yyyymmdd');
    rdq = datenum(rdq, 'yyyymmdd');

    %put nan where data is not available
    rdq(rdq == -9999999) = nan;
    atq(atq == -9999999) = nan;
    dpq(dpq == -9999999) = nan;
    dlttq(dlttq == -9999999) = nan;
    dvpq(dvpq == -9999999) = nan;
    epsfiq(epsfiq == -9999999) = nan;
    epsfxq(epsfxq == -9999999) = nan;
    epspiq(epspiq == -9999999) = nan;
    epspxq(epspxq == -9999999) = nan;
    ibq(ibq == -9999999) = nan;
    intanq(intanq == -9999999) = nan;
    itq(itq == -9999999) = nan;
    niq(niq == -9999999) = nan;
    oiadpq(oiadpq == -9999999) = nan;
    oibdpq(oibdpq == -9999999) = nan;
    req(req == -9999999) = nan;
    revtq(revtq == -9999999) = nan;
    saleq(saleq == -9999999) = nan;
    seqq(seqq == -9999999) = nan;
    teqq(teqq == -9999999) = nan;
    dvy(dvy == -9999999) = nan;
    dvpspq(dvpspq == -9999999) = nan;
    dvpsxq(dvpsxq == -9999999) = nan;
    mkvaltq(mkvaltq == -9999999) = nan;
    prccq(prccq == -9999999) = nan;
    prchq(prchq == -9999999) = nan;
    prclq(prclq == -9999999) = nan;
    oancfy(oancfy == -9999999) = nan;
    ivncfy(oancfy == -9999999) = nan;

    %unique calendar dates available:
    CDATES = unique(datacqtr);
    %delete duplicate values for company identifiers
    LPERMNO = unique(lpermno, 'stable');
    %GVKEY = unique(gvkey, 'stable');
    %TIC = unique(tic, 'stable');
    %COMPNAME = unique(compname, 'stable');
    TIC = [];
    COMPNAME = [];
    
    % Calculate size of target matrices
    %rows - number of calendar dates available
    M = size(CDATES, 1);
    
    %neglect companies with fiscal year not 
    %columns - number of companies
    N = size(LPERMNO, 1);

    % GIC for each company
    GIC = ones(N, 4) * nan;
    
    % Allocate memory for each data item and initialize with nan
    RDQ = ones(M, N) * nan;
    ATQ = ones(M, N) * nan;
    DLTTQ = ones(M, N) * nan;
    DVPQ = ones(M, N) * nan;
    EPSFIQ = ones(M, N) * nan;
    EPSFXQ = ones(M, N) * nan;
    EPSPIQ = ones(M, N) * nan;
    EPSPXQ = ones(M, N) * nan;
    IBQ = ones(M, N) * nan;
    INTANQ = ones(M, N) * nan;
    ITQ = ones(M, N) * nan;
    NIQ = ones(M, N) * nan;
    OIADPQ = ones(M, N) * nan;
    OIBDPQ = ones(M, N) * nan;
    REQ = ones(M, N) * nan;
    REVTQ = ones(M, N) * nan;
    SALEQ = ones(M, N) * nan;
    SEQQ = ones(M, N) * nan;
    TEQQ = ones(M, N) * nan;
    DVY = ones(M, N) * nan;
    DVPSPQ = ones(M, N) * nan;
    DVPSXQ = ones(M, N) * nan;
    MKVALTQ = ones(M, N) * nan;
    PRCCQ = ones(M, N) * nan;
    PRCHQ = ones(M, N) * nan;
    PRCLQ = ones(M, N) * nan;
    OANCFY = ones(M, N) * nan;
    IVNCFY = ones(M, N) * nan;
    
    FISCAL_END = ones(N, 1) * nan;
    GVKEY = ones(N, 1) * nan;
    skipped_i = [];
    i = 1;
    %keeps track of company number. if it changes, we go to the next column in
    %target matrix
    temp_lpermno = lpermno(i);
    TIC = [TIC; tic(i)];
    COMPNAME = [COMPNAME; compname(i)];
    GIC(i, :) = [gsector(i), ggroup(i), gind(i), gsubind(i)];
    %FISCAL_END(i) = fiscal_def(fqtr(i), str2num(datadate(i, 5:6)));
    FISCAL_END(i) = fyr(i);
    GVKEY(i) = gvkey(i);
    
    for j = 1:size(ibq, 1)
        
        if (temp_lpermno ~= lpermno(j))
            %FISCAL_END(i) = fiscal_def(fqtr(j), str2num(datadate(j, 5:6)));
            %if FISCAL_END(i) ~= 12
            %    continue;
            %end
            
            i = i+1;
            FISCAL_END(i) = fyr(j);
            GVKEY(i) = gvkey(j);
            TIC = [TIC; tic(j)];
            COMPNAME = [COMPNAME; compname(j)];
            temp_lpermno = lpermno(j);
            GIC(i, :) = [gsector(j), ggroup(j), gind(j), gsubind(j)];
        end
        
        row_index = find(CDATES == datadates(j));
        
        RDQ(row_index, i) = rdq(j);
        ATQ(row_index, i) = atq(j);
        DPQ(row_index, i) = dpq(j);
        DLTTQ(row_index, i) = dlttq(j);
        DVPQ(row_index, i) = dvpq(j);
        EPSFIQ(row_index, i) = epsfiq(j);
        EPSFXQ(row_index, i) = epsfxq(j);
        EPSPIQ(row_index, i) = epspiq(j);
        EPSPXQ(row_index, i) = epspxq(j);
        IBQ(row_index, i) = ibq(j);
        INTANQ(row_index, i) = intanq(j);
        ITQ(row_index, i) = itq(j);
        NIQ(row_index, i) = niq(j);
        OIADPQ(row_index, i) = oiadpq(j);
        OIBDPQ(row_index, i) = oibdpq(j);
        REQ(row_index, i) = req(j);
        REVTQ(row_index, i) = revtq(j);
        SALEQ(row_index, i) = saleq(j);
        SEQQ(row_index, i) = seqq(j);
        TEQQ(row_index, i) = teqq(j);
        DVY(row_index, i) = dvy(j);
        DVPSPQ(row_index, i) = dvpspq(j);
        DVPSXQ(row_index, i) = dvpsxq(j);
        MKVALTQ(row_index, i) = mkvaltq(j);
        PRCCQ(row_index, i) = prccq(j);
        PRCHQ(row_index, i) = prchq(j);
        PRCLQ(row_index, i) = prclq(j);
        IVNCFY(row_index, i) = ivncfy(j);
        OANCFY(row_index, i) = oancfy(j);
    end
    
    Filter = FISCAL_END == 12 & GIC(:, 3) ~= 40;
    IBQ = IBQ(:,Filter);
    RDQ = RDQ(:,Filter);
    ATQ = ATQ(:,Filter);
    DPQ = DPQ(:,Filter);
    DLTTQ = DLTTQ(:,Filter);
    DVPQ = DVPQ(:,Filter);
    EPSFIQ = EPSFIQ(:,Filter);
    EPSFXQ = EPSFXQ(:,Filter);
    EPSPIQ = EPSPIQ(:,Filter);
    EPSPXQ = EPSPXQ(:,Filter);
    INTANQ = INTANQ(:,Filter);
    ITQ = ITQ(:,Filter);
    NIQ = NIQ(:,Filter);
    OIADPQ = OIADPQ(:,Filter);
    OIBDPQ = OIBDPQ(:,Filter);
    REQ = REQ(:,Filter);
    REVTQ = REVTQ(:,Filter);
    SALEQ = SALEQ(:,Filter);
    SEQQ = SEQQ(:,Filter);
    TEQQ = TEQQ(:,Filter);
    DVY = DVY(:,Filter);
    DVPSPQ = DVPSPQ(:,Filter);
    DVPSXQ = DVPSXQ(:,Filter);
    MKVALTQ = MKVALTQ(:,Filter);
    PRCCQ = PRCCQ(:,Filter);
    PRCHQ = PRCHQ(:,Filter);
    PRCLQ = PRCLQ(:,Filter);
    OANCFY = OANCFY(:,Filter);
    IVNCFY = IVNCFY(:,Filter);
    
    GVKEY = GVKEY(Filter);
    LPERMNO = LPERMNO(Filter);
    COMPNAME = COMPNAME(Filter);
    TIC = TIC(Filter);
    GIC = GIC(Filter, :);
    FISCAL_END = FISCAL_END(Filter);
    
end